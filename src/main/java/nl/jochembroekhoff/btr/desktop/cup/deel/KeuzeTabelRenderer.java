package nl.jochembroekhoff.btr.desktop.cup.deel;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * Render klasse voor de KeuzeTabel. Als er 0 plaatsen zijn, moet de achtergrond
 * lichtgrijs worden. Gebruikt: http://stackoverflow.com/a/24849600/5003260
 *
 * @author Me
 */
@AllArgsConstructor
public class KeuzeTabelRenderer extends DefaultTableCellRenderer {

    @Getter
    private final int plaatsenKolom;

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

        int plaatsen = (Integer) table.getModel().getValueAt(row, getPlaatsenKolom());
        if (plaatsen == 0) {
            setBackground(Color.LIGHT_GRAY);
            setForeground(table.getForeground());
        } else {
            setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
        
        return this;
    }
}
