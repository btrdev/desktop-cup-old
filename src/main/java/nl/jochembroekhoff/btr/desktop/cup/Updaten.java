package nl.jochembroekhoff.btr.desktop.cup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;

/**
 * Automatisch updaten van BtrCup.
 *
 * @author Me
 */
public class Updaten {

    /**
     * Hoe lang de Thread moet wachten voordat er op updates wordt gecontroleerd
     */
    private static final int SLAAPTIJD = 10 * 1000; // 10 seconden
    public static String VERSIE = "?.?.?";
    private static int VERSIE_GETAL = 0;

    public static void initialiseer() {
        IDataopslag dao = BtrApi.getPrefContainer("desktop-cup-updaten");

        int huidigeVersie = dao.getInt("huidige-versie-getal", VERSIE_GETAL);

        //Haal de versie uit de Jar
        try {
            BufferedReader versietxtLezer = new BufferedReader(new InputStreamReader(new Updaten().getClass().getResourceAsStream("/versie.txt")));
            VERSIE = versietxtLezer.readLine();
            VERSIE_GETAL = Integer.valueOf(versietxtLezer.readLine());
        } catch (IOException ex) {
            //Helaas...
            System.out.println("[Updaten]: Foutmelding: " + ex.getMessage());
        }

        //Stel de versie in in het LoginForm
        if (VERSIE_GETAL > huidigeVersie) {
            //Updateproces compleet
            dao.setInt("huidige-versie-getal", VERSIE_GETAL);

            //Geef een melding van de update
            SwingUtilities.invokeLater(() -> {
                JOptionPane.showMessageDialog(null, "BtrCup is geüpdatet\nnaar versie " + Updaten.VERSIE, "Update", JOptionPane.INFORMATION_MESSAGE);
            });
        }

        new Thread(() -> {
            try {
                Thread.sleep(Updaten.SLAAPTIJD);
                //TODO: versie informatie van website verkrijgen en updaten indien nodig
            } catch (InterruptedException ex) {
                //Jammer. Dan maar niet updaten...
            }

            //Check eerst of er een nieuwe versie beschikbaar is
        }).start();
    }
}
