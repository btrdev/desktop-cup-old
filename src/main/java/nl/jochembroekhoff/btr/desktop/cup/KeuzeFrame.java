package nl.jochembroekhoff.btr.desktop.cup;

import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup.ApiCup;
import nl.jochembroekhoff.btr.api.cup.deel.ContacturenLijst;
import nl.jochembroekhoff.btr.api.cup.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ContacturenLijstResultaat;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.desktop.cup.deel.KeuzeFramePaneel;

/**
 * KeuzeFrame. Het hoofdscherm voor interactie.
 *
 * @author Me
 */
public class KeuzeFrame extends javax.swing.JFrame {

    private Map<String, List<Contactuur>> keuzes = new HashMap<>();
    private Map<String, KeuzeFramePaneel> panelen = new HashMap<>();
    private List<String> panelenSorteerlijst = new ArrayList<>();

    private boolean ladend = true;

    /**
     * KeuzeFrame constructor
     */
    public KeuzeFrame() {
        initComponents();

        //icoontje instellen
        try {
            this.setIconImage(
                    ImageIO.read(getClass().getClassLoader().getResource("icon.png"))
            );
        } catch (IOException ex) {
            //ok
        }
    }

    /**
     * UI componenten door NetBeans.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitel = new javax.swing.JLabel();
        lblJijBent = new javax.swing.JLabel();
        lblNaam = new javax.swing.JLabel();
        btnLogUit = new javax.swing.JButton();
        sep1 = new javax.swing.JSeparator();
        btnPrintbaarRooster = new javax.swing.JButton();
        tabsLessen = new javax.swing.JTabbedPane();
        dropWeek = new javax.swing.JComboBox<>();
        btnVernieuw = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Contacturen Overzicht - BtrCup");

        lblTitel.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblTitel.setText("Maak je keuze");

        lblJijBent.setText("Jij bent:");

        lblNaam.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblNaam.setText("A. Naam");

        btnLogUit.setText("Log Uit");
        btnLogUit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogUitActionPerformed(evt);
            }
        });

        btnPrintbaarRooster.setText("Printbaar Rooster");
        btnPrintbaarRooster.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintbaarRoosterActionPerformed(evt);
            }
        });

        dropWeek.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Week 39", "Week 40", "Week 41" }));
        dropWeek.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                dropWeekItemStateChanged(evt);
            }
        });

        btnVernieuw.setText("Vernieuw");
        btnVernieuw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVernieuwActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblJijBent)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNaam, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dropWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPrintbaarRooster)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnVernieuw)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLogUit))
                    .addComponent(tabsLessen, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addComponent(sep1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitel)
                    .addComponent(btnLogUit)
                    .addComponent(btnPrintbaarRooster)
                    .addComponent(dropWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVernieuw))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblJijBent)
                    .addComponent(lblNaam))
                .addGap(7, 7, 7)
                .addComponent(sep1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabsLessen, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintbaarRoosterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintbaarRoosterActionPerformed
        JOptionPane.showMessageDialog(this, "Hier wordt nog aan gewerkt.\nHet programma werkt automatisch bij,\ndus binnenkort zal het beschikbaar zijn!", "Helaas...", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnPrintbaarRoosterActionPerformed

    private void btnLogUitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogUitActionPerformed
        ApiCup.verwijderOpgeslagenGegevens();
        System.exit(0);
    }//GEN-LAST:event_btnLogUitActionPerformed

    private void dropWeekItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dropWeekItemStateChanged
        //Als er een week wordt geselecteerd ...
        if (evt.getStateChange() == ItemEvent.SELECTED && !ladend) {
            laadPanelen();
        }
    }//GEN-LAST:event_dropWeekItemStateChanged

    private void btnVernieuwActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVernieuwActionPerformed
        // TODO: Alles vernieuwen
    }//GEN-LAST:event_btnVernieuwActionPerformed

    private void laadData(LogInResultaat lir) {
        //Ladend ...
        ladend = true;

        //Naam laden
        lblNaam.setText(ApiCup.getGebruiker());

        //Document inladen
        ContacturenLijstResultaat clr = ContacturenLijst.contacturenLijst(lir.getDocument());
        Map<Integer, Map<String, Contactuur>> wekenOngesorteerd = clr.getResultaat();

        //Sorteer de weken in wekenGesorteerd
        List<Integer> wekenGesorteerd = new ArrayList(wekenOngesorteerd.keySet());
        Collections.sort(wekenGesorteerd);

        //Maak de dropdown en panelen leeg
        dropWeek.removeAllItems();
        int weeknummerDezeWeek = BtrApi.getWeeknummerUitDatum(new Date());
        panelen.clear();

        List<Integer> weeknummers = new ArrayList<>();

        //Loop over alle weken
        wekenGesorteerd.forEach((weeknummer) -> {
            //Voeg de weken toe in de dropdown
            dropWeek.addItem("Week " + weeknummer);

            weeknummers.add(weeknummer);

            Map<String, Contactuur> contacturen = wekenOngesorteerd.get(weeknummer);
            contacturen.forEach((String lesuur, Contactuur contactuur) -> {
                int lesnummer = contactuur.getLesnummer();
                int dagnr = contactuur.getLesdatum().getDay(); /*Dagnummer toevoegen voor sortering!*/
                String paneelNaam = "week-" + weeknummer + "_les-" + dagnr + lesuur;
                String uniekId = ApiCup.getGebruiker() + "|" + (contactuur.getLesdatum().getYear() + 1900) + "|" + paneelNaam; //Gebruikt voor notities o.a.
                
                panelen.put(paneelNaam, new KeuzeFramePaneel(contactuur, uniekId));
            });

            //Sorteer de panelen netjes
            panelenSorteerlijst = new ArrayList(panelen.keySet());
            Collections.sort(panelenSorteerlijst);
        });

        boolean erZijnEchtWeken = true;
        //Als er nog geen item is geselecteerd... Selecteer de eerste
        if (dropWeek.getSelectedIndex() < 0) {
            //Als we wel daadwerkelijk items zijn ...
            if (dropWeek.getItemCount() > 0) {
                //.. selecteer de eerste week dan maar
                dropWeek.setSelectedIndex(0);
            } else {
                erZijnEchtWeken = false;
            }
        }

        //Als de weken er ook echt zijn, simuleer een klik op dropWeek om data
        //in te laden in de tabs
        if (erZijnEchtWeken) {
            //Ingenieus werk: selecteer de dichtstbijzijnde week (in het geval van vakantie bijvoorbeeld)
            dropWeek.setSelectedItem("Week " + BtrApi.getDichtstbijzindeInt(weeknummerDezeWeek, weeknummers));
            ladend = false;
            laadPanelen();
        }

        ladend = false;
    }

    /**
     * Verkrijg een nieuwe instantie van KeuzeFrame. De data wordt geladen uit
     * het LogInResultaat.
     *
     * @param lir het LogInResultaat van de API
     * @return KeuzeFrame met data - null in het geval van een fout
     */
    public static KeuzeFrame laadVanApi(LogInResultaat lir) {
        KeuzeFrame frame = new KeuzeFrame();
        frame.laadData(lir);
        return frame;
    }

    private void laadPanelen() { /* Of voeg de tabs toe */
        System.out.println("Panelen laden ...");

        //Maak de tabs leeg
        //TODO: tabs leegmaken
        tabsLessen.removeAll();

        String geselecteerdeWeek = (String) dropWeek.getSelectedItem();
        geselecteerdeWeek = geselecteerdeWeek.toLowerCase();
        geselecteerdeWeek = geselecteerdeWeek.replace(" ", "-");

        //Loop over alle contacturen (gesorteerd!)
        for (String paneelnaam : panelenSorteerlijst) {
            //Als het contactuur start met de week ...
            if (paneelnaam.startsWith(geselecteerdeWeek)) {
                KeuzeFramePaneel paneel = panelen.get(paneelnaam);
                System.out.println("Paneelnaam="+paneelnaam);
                tabsLessen.addTab(ApiCup.getLesuurNaam(paneel.getContactuur()), paneel);
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogUit;
    private javax.swing.JButton btnPrintbaarRooster;
    private javax.swing.JButton btnVernieuw;
    private javax.swing.JComboBox<String> dropWeek;
    private javax.swing.JLabel lblJijBent;
    private javax.swing.JLabel lblNaam;
    private javax.swing.JLabel lblTitel;
    private javax.swing.JSeparator sep1;
    private javax.swing.JTabbedPane tabsLessen;
    // End of variables declaration//GEN-END:variables
}
