package nl.jochembroekhoff.btr.desktop.cup;

import java.util.prefs.Preferences;
import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;
import nl.jochembroekhoff.btr.api.dataopslag.Prefroot;

/**
 *
 * @author Jochem
 */
public class DataopslagDesktop extends IDataopslag {

    private final Preferences prefs;

    public DataopslagDesktop(String containerNaam) {
        super(containerNaam);
        this.prefs = Preferences.userNodeForPackage(Prefroot.class).node(containerNaam);
    }

    @Override
    public void clear() {
        try {
            prefs.clear();
        } catch (Exception e) {
            e.printStackTrace();
            //TODO
        }
    }

    @Override
    public String get(String key) {
        return get(key, null);
    }

    @Override
    public String get(String key, String standaard) {
        return prefs.get(key, standaard);
    }

    @Override
    public int getInt(String key) {
        return getInt(key, -1);
    }

    @Override
    public int getInt(String key, int standaard) {
        return prefs.getInt(key, standaard);
    }

    @Override
    public void set(String key, String waarde) {
        prefs.put(key, waarde);
    }

    @Override
    public void setInt(String key, int waarde) {
        prefs.putInt(key, waarde);
    }

}
