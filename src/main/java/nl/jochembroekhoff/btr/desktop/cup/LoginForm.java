package nl.jochembroekhoff.btr.desktop.cup;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import nl.jochembroekhoff.btr.api.BtrApi;
import org.apache.commons.io.FileUtils;
import nl.jochembroekhoff.btr.api.cup.ApiCup;
import nl.jochembroekhoff.btr.api.cup.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ZoekNamenResultaat;
import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;

/**
 *
 * @author Me
 */
public class LoginForm extends javax.swing.JFrame {

    /**
     * Pincodedialoog
     */
    DialogPincode dp = new DialogPincode(this, true);
    /**
     * Onthouden of de laatste zoekactie succesvol was
     */
    private boolean laatsteZoekSuccesvol = false;

    /**
     * Creates new form LoginForm
     */
    public LoginForm() {
        initComponents();

        //Versie
        lblVersieOndertitel.setText("versie " + Updaten.VERSIE);

        //icoontje instellen
        try {
            this.setIconImage(
                    ImageIO.read(getClass().getClassLoader().getResource("icon.png"))
            );

            //Checken of de gebruiker het waarschuwingsbericht heeft gelezen
            IDataopslag gebrpref = BtrApi.getPrefContainer("desktop-cup-overig");
            if (gebrpref.getInt("waarschuwing_accepteer", 0) == 0) {
                //TODO: Minder omslachtige manier vinden
                String waarschuwingBericht = FileUtils.readFileToString(new File(getClass().getClassLoader().getResource("waarschuwing.txt").getPath()));
                JOptionPane.showMessageDialog(this, waarschuwingBericht, "Voor je verder gaat", JOptionPane.WARNING_MESSAGE);
                gebrpref.setInt("waarschuwing_accepteer", 1);
            }
        } catch (IOException ex) {
            //ok, jammer dan
        }
    }

    /**
     * UI code.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        lstNamen = new javax.swing.JList<>();
        txtZoekcode = new javax.swing.JTextField();
        btnZoek = new javax.swing.JButton();
        btnVerder = new javax.swing.JButton();
        lblTitel = new javax.swing.JLabel();
        lblVersieOndertitel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login - BtrCup");

        lstNamen.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Vul zoekcode in", "en klik op Zoek", ".", "Selecteer dan uit lijst", "en klik op Verder" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstNamen.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstNamen.setEnabled(false);
        lstNamen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstNamenMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstNamen);

        txtZoekcode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtZoekcodeKeyReleased(evt);
            }
        });

        btnZoek.setText("Zoek");
        btnZoek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZoekActionPerformed(evt);
            }
        });

        btnVerder.setText("Verder");
        btnVerder.setEnabled(false);
        btnVerder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerderActionPerformed(evt);
            }
        });

        lblTitel.setFont(lblTitel.getFont().deriveFont(lblTitel.getFont().getSize()+1f));
        lblTitel.setText("BtrCup");

        lblVersieOndertitel.setFont(lblVersieOndertitel.getFont());
        lblVersieOndertitel.setText("versie ?");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnZoek, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnVerder, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblTitel)
                    .addComponent(lblVersieOndertitel)
                    .addComponent(txtZoekcode, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblVersieOndertitel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtZoekcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnZoek)
                            .addComponent(btnVerder))))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnZoekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZoekActionPerformed
        //Forceer de API te herstarten om problemen te voorkomen.
        ApiCup.start();

        //Zoeken en in lijst plaatsen
        String zoekletters = txtZoekcode.getText();
        ZoekNamenResultaat znr = ZoekNamen.zoek(zoekletters);

        //lstNamen instellen op gelukt of niet
        //en ook laatsteZoekSuccesvol (om de btnVerder te laten zien of niet)
        lstNamen.setEnabled(znr.gelukt());
        laatsteZoekSuccesvol = znr.gelukt();
        //btnVerder uitschakelen, omdat de listhandler die automatisch regelt
        btnVerder.setEnabled(false);

        DefaultListModel m = new DefaultListModel();
        if (znr.gelukt()) {

            //Zoekletters opslaan (bij voorbaat)
            ApiCup.setGebruikerZoekletters(zoekletters);

            //Alle namen toevoegen aan de lijst
            znr.getResultaat().forEach((naam) -> {
                m.addElement(naam);
            });

        }
        //Altijd de lijst updaten, het kan gebeuren dat er geen resultaten zijn
        lstNamen.setModel(m);
    }//GEN-LAST:event_btnZoekActionPerformed

    private void btnVerderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerderActionPerformed
        //Pincode dialoog tonen
        dp.voorbereiden();
        dp.setVisible(true);
    }//GEN-LAST:event_btnVerderActionPerformed

    /**
     * Handler voor het klikken op de zoeklijst.
     *
     * @param evt
     */
    private void lstNamenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstNamenMouseClicked
        if (laatsteZoekSuccesvol) {
            btnVerder.setEnabled(true);
            ApiCup.setGebruiker(lstNamen.getSelectedValue());

            //Doe alsof de Verder knop wordt ingedrukt als er dubbel wordt
            //geklikt op een lijstitem
            if (evt.getClickCount() == 2) {
                btnVerderActionPerformed(null);
            }
        }
    }//GEN-LAST:event_lstNamenMouseClicked

    private void txtZoekcodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtZoekcodeKeyReleased
        //Als op de Enter toets wordt gedrukt ...
        if (evt.getKeyCode() == 10 && txtZoekcode.getText().length() >= 3) {
            //...doen alsof de btnZoek wordt aangeklikt
            btnZoekActionPerformed(null);
        }
    }//GEN-LAST:event_txtZoekcodeKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        //LAF instellen
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {//helaas...
            e.printStackTrace();
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            boolean nogInloggen = true;

            //Cupweb instellingen instellen
            BtrApi.setDatamanager(DataopslagDesktop.class);
            ApiCup.setCupUrl("https://ccgobb.cupweb6.nl");
            ApiCup.start();

            //Updaten (NA driverbase, VOOR gui)
            Updaten.initialiseer();

            if (ApiCup.kanAutomatischInloggen()) {
                LogInResultaat lir = ApiCup.logAutomatischIn();
                if (lir.gelukt()) {
                    nogInloggen = false;

                    //Laad het KeuzeFrame direct
                    KeuzeFrame.laadVanApi(lir).setVisible(true);
                }
            }
            if (nogInloggen) {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerder;
    private javax.swing.JButton btnZoek;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTitel;
    private javax.swing.JLabel lblVersieOndertitel;
    private javax.swing.JList<String> lstNamen;
    private javax.swing.JTextField txtZoekcode;
    // End of variables declaration//GEN-END:variables
}
